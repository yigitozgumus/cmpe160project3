General Information and How to Run the Code :

 	1. To run the program, run the GUI.java class.
 	
 	2. The location of the maze file should be at the same directory of this README file.
 	
 	3. There are two test mazes included to the project for debugging and the filename that is written by default 
 	is "maze.txt" , so before testing either add the file to the correct directory and type the new name* of the maze 
 	file or delete the current maze.txt and copy your test maze file to the correct directory with the name "maze.txt".
 	
 	4.(*) the name of the maze file that will be tested should be written in the gamePanel 
 	initialization line in the initialize method in the GUI class(line 58 in GUI.java).The line is surrounded with 
 	comments and a TODO tag to spot easily.  
 	
 	5.  If you want to disable the animation of the program just make isAnimation boolean variable false in the 
 	MazeSolver class. It has a TODO tag for convenience
 	
 	Project consists of 4 classes and 1 interface. It draws the solution path with animation so you can increase
 	the frameRate if the maze is too complex. Window is resizable and has a scrollpane just in case.
 	There is also a printed version of the maze with represented integers in the console that, -1's are walls 
 	unvisited nodes are 32000 and other numbers are the amount of nodes you have to visit from the source to 
 	reach that specific node. It was the bit of code that when I first decide to implement djikstra's algorithm 
 	but after I implement the BFS algorithm, I decided to keep that part to help me debug the code or the maze 
 	if needed. 
 	
 	
 	
 	