//package cmpe160Project3;
/** Interface for the animation part of the program
 * 
 * */
public interface Game {
	/** Empty method for the setup of the gamePanel of the program. Takes a GamePanel object.
	 * 
	 * @param panel GamePanel object of the current MazeSolver object
	 * */
	public void setGamePanel(GamePanel panel);
	
	/** Empty method for the animation of the program.
	 * 
	 * */
	public void animateObjects();
}