//package cmpe160Project3;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

/** Class for the visual representation of the program
 *
 * */
public class GUI {

	private JFrame frame;
	private GamePanel gamePanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 *
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(true);
		frame.setBounds(100,100,500, 520);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//--------------------------------------------------------------------------
		//----------------------------------!!!MAZE NAME HERE !!!-------------------
		gamePanel = new GamePanel(new MazeSolver("maze2.txt"));//TODO
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		gamePanel.setBackground(Color.WHITE);
		frame.getContentPane().add(gamePanel);
		frame.pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		if(width < frame.getWidth() || height < frame.getHeight()){
		Container contain = frame.getContentPane();
		JScrollPane scrollBar = new JScrollPane(gamePanel);
		 contain.add(scrollBar);
		 }
	}

}
/** Inner class for the Panel creation of the Program
 *
 * */
class GamePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private int gridSquareSize = 15;

	protected int gamePanelWidth  ;
	protected int gamePanelHeight ;

	private BufferedImage gameImage;
	private Timer gameTimer;

	private int frameRate =5;

	private Game game;
	/** Creates a gamePanel object. Takes one Game object.
	 *
	 * @param game Game object of the panel.
	 * */
	public GamePanel(Game game) {
		super();

		game.setGamePanel(this);
		this.game = game;
		gameImage = new BufferedImage(gamePanelWidth , gamePanelHeight , BufferedImage.TYPE_INT_ARGB);
		setGameTimer();
	}
	/** starts the timer of the program for the animation effect.
	 *
	 * */
	private void setGameTimer() {
		gameTimer = new Timer(100/frameRate, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				animate();
				repaint();
			}
		});
		gameTimer.setInitialDelay(0);
		gameTimer.start();
	}
	/** This method is constantly called during the execution of the program.
	 *
	 * */
	private void animate() {
		clearCanvas();
		drawGrid();

		game.animateObjects();
	}
	/** Sets the new Grid size for the Panel.
	 *
	 * */
	public void setGridSize(int gridWidth, int gridHeight) {
		this.gamePanelWidth = gridWidth * gridSquareSize;
		this.gamePanelHeight = gridHeight * gridSquareSize;
	}
	/** Gets the current dimension information of the panel.
	 *
	 * */
	public Dimension getPreferredSize() {
		return new Dimension(gamePanelWidth, gamePanelHeight);
	}
	/** Precaution in case maze is too big for the screen.
	 *
	 * */
	public void setGridSquareSize(int gridSize){
		gridSquareSize = gridSize ;
	}
	/** Paint method for the game image.
	 *
	 * */
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (gameImage != null) {
			g.drawImage(gameImage, 0, 0, null);
		}
	}
	/** Clears canvas for update.
	 *
	 * */
	private void clearCanvas() {
		Graphics g = gameImage.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, gameImage.getWidth(), getHeight());
		g.dispose();
	}
	/** getter method for the gridWidth.
	 *
	 * */
	public int getGridWidth() {
		return gamePanelWidth/gridSquareSize ;
	}
	/** Getter method for gridHeight.
	 *
	 * */
	public int getGridHeight() {
		return gamePanelHeight/gridSquareSize ;
	}
	/** Basically draws the cells.
	 *
	 * */
	private void drawGrid() {
		Graphics g = gameImage.getGraphics();
		g.setColor(Color.LIGHT_GRAY);

		// vertical grid
		for (int i=0; i<getGridWidth(); i++) {
			int lineX = i*gridSquareSize;
			g.drawLine(lineX, 0, lineX, gamePanelHeight);
		}
		// horizontal grid
		for (int i=0; i<getGridHeight(); i++) {
			int lineY = i*(gridSquareSize);
			g.drawLine(0, lineY, gamePanelWidth, lineY);
		}
		g.dispose();
	}
	/** Drawing method for the normal square.
	 *
	 * */
	public void drawRectangle(int gridX, int gridY, Color color) {
		if (gridX < 0 || gridY < 0 ||
				gridX >= getGridWidth() || gridY >= getGridHeight()) {
			return;
		}
		Graphics g = gameImage.getGraphics();
		g.setColor(color);
		int x = gridX*gridSquareSize+1;
		int y = gridY*gridSquareSize+1;
		g.fillRect(x, y, gridSquareSize-1, gridSquareSize-1);
		g.dispose();
	}
	/** Drawing method for the small square.
	 *
	 * */
	public void drawSmallRectangle(int gridX, int gridY, Color color) {
		if (gridX < 0 || gridY < 0 ||
				gridX >= getGridWidth() || gridY >= getGridHeight()) {
			return;
		}
		Graphics g = gameImage.getGraphics();
		g.setColor(color);
		int x = gridX*gridSquareSize+3;
		int y = gridY*gridSquareSize+3;
		g.fillRect(x, y, gridSquareSize-5, gridSquareSize-5);
		g.dispose();
	}
}
