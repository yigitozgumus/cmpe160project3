//package cmpe160Project3;

import java.awt.Color;
import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
/** Main class of the program in terms of implementation. 
 *  Contains the main drawing methods and the searching algorithm.
 *  Implements Game interface. 
 * */
public class MazeSolver implements Game {

	private int numberOfLines ;
	private int numberOfRows ;
	private GamePanel gamePanel ;
	private ArrayList<String> rows = new ArrayList<String>();
	private MazeNode[][] map ;
	private Stack<MazeNode> thePath = new Stack<MazeNode>();
	private Queue<MazeNode> algorithmSack = new LinkedList<MazeNode>();
	private static MazeNode startNode ;
	Scanner input;
	
	private boolean isAnimation = true ;//TODO

	/** Constructor of the MazeSolver class. Creates a mazeSolver object to find the path of the given maze.
	 *  Takes a String argument to collect the name of the file.
	 *  
	 *  @param fileName file name of the given maze
	 * */
	public MazeSolver(String fileName) {
		try {
			input = new Scanner(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/** Sets the window of the program and controls the main building process. The building block methods of the class
	 *  is called in this method. Method first constructs the window with adjustments regarding the dimensions of the window if 
	 *  the maze is too large. Then searching algoritm is called and for debugging purposes the alternate version of the maze
	 *  (with their shortest cost values as node feature) is printed for debugging and more information about the maze.
	 *  Takes a GamePanel object.
	 *  
	 *  @param panel GamePanel object of the window.
	 * */
	@Override
	public void setGamePanel(GamePanel panel) {
		this.gamePanel = panel;
		try {
			constructWindow(input,rows);
			gamePanel.setGridSize(numberOfRows,numberOfLines);

			Dimension check = gamePanel.getPreferredSize();
			if(check.width > 2000 || check.height > 1600){
				gamePanel.setGridSquareSize(6);
				gamePanel.setGridSize(numberOfRows,numberOfLines);
			} else if(check.width > 1200 || check.height > 800){
				gamePanel.setGridSquareSize(10);
				gamePanel.setGridSize(numberOfRows,numberOfLines);
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		map = new MazeNode[numberOfLines][numberOfRows];
		constructMap(rows,map);
		searchThePath(map, thePath, algorithmSack);
		// just for extra information
		for(int i = 0 ; i < map.length ; i++){
			for(int j = 0 ; j < map[0].length;j++){
				System.out.printf("%4d",map[i][j].getRoadSoFar() );
				System.out.print(" ");
			}
			System.out.println();	
		}
	}
	/** Paints the maze with updates at each turn of the timer object at the GUI class.
	 *  Calls drawMap method.
	 * */
	@Override
	public void animateObjects() {
		drawMap(rows);
	}
	/** Gets the necessary elements for the window creation. Takes a Scanner object and an Arraylist containing
	 *  the Strings of the maze document. Returns nothing.
	 * */
	public void constructWindow (Scanner s,ArrayList<String> list) throws FileNotFoundException{

		while(input.hasNextLine()){
			list.add(input.nextLine());
			setNumberOfLines(getNumberOfLines()+1);
		}
		setNumberOfRows(list.get(0).length());
	}
	/** Creates the map of the maze for the searchPath method. It reads the characters with a nested loop and
	 *  creates mazeNodes for each type of node at their appropriate places. Takes a ArrayList object and Multidimensional
	 *  array of MazeNode object. Returns Nothing
	 *  
	 *  @param list storage of the characters.
	 *  @param map MazeNode multidimensional representation of the given maze document.
	 * 
	 * */
	public void constructMap(ArrayList<String> list,MazeNode[][] map){
		for(int rowIndex = 0 ; rowIndex < list.size();rowIndex++ ){
			char[] lineArray = list.get(rowIndex).toCharArray();
			for(int columnIndex = 0 ; columnIndex < lineArray.length;columnIndex++){
				switch(lineArray[columnIndex]){
				case ' ':
					map[rowIndex][columnIndex] = new MazeNode(false,false,false,32000,columnIndex,rowIndex);
					break;
				case 'E':
					map[rowIndex][columnIndex] = new MazeNode(false,false,true,32000,columnIndex,rowIndex);
					break;
				case 'S':
					map[rowIndex][columnIndex] = new MazeNode(false,false,false,0,columnIndex,rowIndex);
					startNode = map[rowIndex][columnIndex];
					break;
				case 'x':
					map[rowIndex][columnIndex] = new MazeNode(true,false,false,-1,columnIndex,rowIndex);
					break;
				default:
					break;
				}
			}
		}
	}
	/** Draws the map of the maze. First Switch case draws the walls, beginning and the end. Second if block draws the
	 *  solution path when it is found. Takes an ArrayList object and returns void.
	 * 
	 * @param list text representation of the maze in ArrayList form.
	 * */
	public void drawMap(ArrayList<String> list){
		for(int rowIndex = 0 ; rowIndex < list.size(); rowIndex++ ){
			char[] lineArray = list.get(rowIndex).toCharArray();
			for(int lineIndex = 0 ; lineIndex < lineArray.length;lineIndex++){
				switch(lineArray[lineIndex]){
				case 'E':
					gamePanel.drawRectangle(lineIndex, rowIndex, Color.RED);break;
				case 'S':
					gamePanel.drawRectangle(lineIndex, rowIndex, Color.GREEN);break;
				case 'x':
					gamePanel.drawRectangle(lineIndex, rowIndex, Color.BLACK);break;
				default:
					break;
				}
				if(map[rowIndex][lineIndex].isSolution == true && map[rowIndex][lineIndex] != startNode &&
						!map[rowIndex][lineIndex].isEnd ){
					gamePanel.drawRectangle(lineIndex, rowIndex, Color.cyan);
				}
			}
		}
		if(isAnimation){
			if(!thePath.isEmpty()){
				MazeNode current = thePath.pop();
				current.isSolution = true ;
			}
		}

	}

	/** Gets the shortest path from the start to the end if such way exists. Takes a MazeNode multidimensional array ,
	 *  a Stack to save the solution path and a Queue for the algorithm implementation. Method implements a Breadth first search algorithm
	 *  Also implements shortest cost distance method of the dijkstra's algorithm but that part is exist as an extra information.
	 *   
	 *  
	 *  @param map structured map of the maze.
	 *  @param finalPath holds the shortest path of the maze if it exists.
	 *  @param algorithmSack is used in the BFS implementation of the method.
	 * 
	 * */
	public void searchThePath(MazeNode[][] map,Stack<MazeNode> finalPath,Queue<MazeNode> algorithmSack){
		MazeNode current ;
		MazeNode pathNode = null ;
		algorithmSack.add(startNode);
		startNode.setVisited(true);
		startNode.setPredecessor(null);
		while(!algorithmSack.isEmpty()){ 
			current = algorithmSack.poll();
			if(current.isEnd()){ 
				pathNode = current ;
				while(pathNode != null){
					if(!isAnimation){
						pathNode.isSolution = true ;
					}
					thePath.add(pathNode);
					pathNode = pathNode.getPredecessor();
				}
				break;
			}
			if(current.x-1>= 0){
				if(map[current.y][current.x-1].isVisited() == false && map[current.y][current.x-1].isWall() == false){
					algorithmSack.add(map[current.y][current.x-1]);
					map[current.y][current.x-1].setVisited(true);
					map[current.y][current.x-1].setRoadSoFar(Math.min(current.getRoadSoFar()+1, map[current.y][current.x-1].getRoadSoFar()));
					map[current.y][current.x-1].setPredecessor(current);
				}				
			} if(current.y-1>= 0){
				if(map[current.y-1][current.x].isVisited() == false && map[current.y-1][current.x].isWall() == false){
					algorithmSack.add(map[current.y-1][current.x]);
					map[current.y-1][current.x].setVisited(true);
					map[current.y-1][current.x].setRoadSoFar(Math.min(current.getRoadSoFar()+1, map[current.y-1][current.x].getRoadSoFar()));
					map[current.y-1][current.x].setPredecessor(current);
				}

			} if(current.x+1<= map[0].length-1){
				if(map[current.y][current.x+1].isVisited() == false && map[current.y][current.x+1].isWall() == false){
					algorithmSack.add(map[current.y][current.x+1]);
					map[current.y][current.x+1].setVisited(true);
					map[current.y][current.x+1].setRoadSoFar(Math.min(current.getRoadSoFar()+1, map[current.y][current.x+1].getRoadSoFar()));
					map[current.y][current.x+1].setPredecessor(current);
				}

			} if(current.y+1<= map.length-1){
				if(map[current.y+1][current.x].isVisited() == false && map[current.y+1][current.x].isWall() == false){
					algorithmSack.add(map[current.y+1][current.x]);
					map[current.y+1][current.x].setVisited(true);
					map[current.y+1][current.x].setRoadSoFar(Math.min(current.getRoadSoFar()+1, map[current.y+1][current.x].getRoadSoFar()));
					map[current.y+1][current.x].setPredecessor(current);
				}
			}
		}



	}
	/** Getter method for the numberOfLines variable.
	 * 
	 * @return numberOfLines
	 * */
	public int getNumberOfLines() {
		return numberOfLines;
	}
	/**Setter method for the numberOfLines variable. Takes an integer parameter.
	 * 
	 * @param numberOfLines number of lines for the maze window.
	 * */
	public void setNumberOfLines(int numberOfLines) {
		this.numberOfLines = numberOfLines;
	}
	/** Getter method for the numberOfRows variable.
	 * 
	 * @return numberOfRows
	 * */
	public int getNumberOfRows() {
		return numberOfRows;
	}
	/**Setter method for the numberOfRows variable. Takes an integer parameter.
	 * 
	 * @param numberOfRows number of lines for the maze window.
	 * */
	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}
}
