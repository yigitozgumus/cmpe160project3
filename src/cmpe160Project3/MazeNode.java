//package cmpe160Project3;
/** This class represents the nodes of the maze for the pathfinding.
 * */
public class MazeNode {

	private boolean isWall;
	private boolean isVisited ;
	private int roadSoFar ;
	boolean isEnd ;
	protected MazeNode predecessor = null  ;
	protected int x ;
	protected int y ;
	protected boolean isSolution = false ;


	//new MazeNode constructor
	/** Creates a MazeNode object. Takes total of six parameters. Three of them are booleans for situation checks and 
	 *  the other three are integers. which hold both vertical and horizontal location and the distance between the node and
	 *  the source node
	 *  
	 *  @param isWall boolean to check that if the node is a wall
	 *  @param isVisited boolean to check that if the node is visited before
	 *  @param isEnd checks if it is thefinal node
	 *  @param roadSoFar distance between the node and the source,in terms of nodes
	 *  @param x horizontal location
	 *  @param y vertical location
	 *  
	 * */
	public MazeNode(boolean isWall,boolean isVisited,boolean isEnd,int roadSoFar,int x,int y) {
		this.isWall = isWall;
		this.isVisited = isVisited;
		this.isEnd = isEnd ;
		this.roadSoFar = roadSoFar ;
		this.x = x ;
		this.y = y ;
	}
	/** Checks if the node is a wall node.
	 * */
	public boolean isWall() {
		return isWall;
	}
	/** Checks if the node is visited before.
	 * */
	public boolean isVisited() {
		return isVisited;
	}
	/** Changes the boolean value if the visited state changes.
	 * */
	public void setVisited(boolean isVisited) {
		this.isVisited = isVisited;
	}
	/** Returns the distance between the node and the start node.
	 * */
	public int getRoadSoFar() {
		return roadSoFar;
	}
	/** Changes the value of the distance accordingly. Takes an integer and returns void.
	 * 
	 * @param roadSoFar the new distance value
	 * */
	public void setRoadSoFar(int roadSoFar) {
		this.roadSoFar = roadSoFar;
	}
	/** Checks if the node is the final node. Returns a boolean value.
	 * */
	public boolean isEnd() {
		return isEnd;
	}
	/** Gets the parent node of the current node. Returns a MazeNode object.
	 * 
	 * */
	public MazeNode getPredecessor() {
		return predecessor;
	}
	/** Sets the parent of the current node. Takes one MazeNode parameter.
	 * 
	 * @param predecessor the new parent of the node
	 * */
	public void setPredecessor(MazeNode predecessor) {
		this.predecessor = predecessor;
	}

}
